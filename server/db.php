<?php

class Database 
{
	private static $cont  = null;
	
	public function __construct() {
		exit('Init function is not allowed');
	}
	


	public static function connect()
	{
	 $config = parse_ini_file("Config.ini");
		if ( null == self::$cont )
	       {      
		try 
		{       
			//self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword); 
		self::$cont =  new PDO( "mysql:host=".$config["db_host"].";"."dbname=".$config["db_name"], $config["db_user"], $config["db_password"]);			
		}
		catch(PDOException $e) 
		{
		  die($e->getMessage());  
		}
	       } 
	       return self::$cont;
	}

	
	
	public static function disconnect()
	{
		self::$cont = null;
	}
}

?>
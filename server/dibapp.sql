-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versione server:              10.4.6-MariaDB - mariadb.org binary distribution
-- S.O. server:                  Win64
-- HeidiSQL Versione:            10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dump della struttura del database dibapp
CREATE DATABASE IF NOT EXISTS `dibapp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dibapp`;

-- Dump della struttura di tabella dibapp.commenti
CREATE TABLE IF NOT EXISTS `commenti` (
  `lezione` int(11) DEFAULT NULL,
  `valutazione` int(11) DEFAULT NULL,
  `messaggio` varchar(200) DEFAULT NULL,
  `pubblico` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella dibapp.commenti: ~4 rows (circa)
/*!40000 ALTER TABLE `commenti` DISABLE KEYS */;
INSERT INTO `commenti` (`lezione`, `valutazione`, `messaggio`, `pubblico`) VALUES
	(11, 4, 'Questo e un commento inviato da uno studente', 1),
	(11, 4, 'Questo e un commento inviato da uno studente', 1),
	(11, 4, 'Questo e un commento inviato da uno studente', 1),
	(11, 4, 'Questo e un commento inviato da uno studente', 1);
/*!40000 ALTER TABLE `commenti` ENABLE KEYS */;

-- Dump della struttura di tabella dibapp.lezioni
CREATE TABLE IF NOT EXISTS `lezioni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materia` int(11) DEFAULT NULL,
  `day` date NOT NULL DEFAULT curdate(),
  `orario_start` time DEFAULT NULL,
  `orario_end` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella dibapp.lezioni: ~8 rows (circa)
/*!40000 ALTER TABLE `lezioni` DISABLE KEYS */;
INSERT INTO `lezioni` (`id`, `materia`, `day`, `orario_start`, `orario_end`) VALUES
	(1, 1, '2020-01-20', '08:00:09', '23:59:21'),
	(2, 1, '2020-01-21', '08:00:09', '23:59:21'),
	(3, 1, '2020-01-22', '08:00:09', '23:59:21'),
	(4, 1, '2020-01-23', '08:00:09', '23:59:21'),
	(5, 1, '2020-01-24', '08:00:09', '23:59:21'),
	(6, 1, '2020-01-25', '08:00:09', '23:59:21'),
	(7, 1, '2020-01-26', '08:00:09', '23:59:21'),
	(8, 1, '2020-01-27', '08:00:09', '23:59:21'),
	(9, 1, '2020-01-28', '08:00:09', '23:59:21'),
	(10, 1, '2020-01-29', '08:00:09', '23:59:21'),
	(11, 1, '2020-02-05', '08:00:09', '23:59:21'),
	(12, 1, '2020-02-06', '08:00:09', '23:59:21'),
	(13, 1, '2020-02-06', '08:00:09', '23:59:21'),
	(14, 1, '2020-02-07', '08:00:09', '23:59:21'),
	(15, 1, '2020-02-08', '08:00:09', '23:59:21'),
	(16, 1, '2020-02-11', '08:00:09', '23:59:21'),
	(17, 1, '2020-02-12', '08:00:09', '23:59:21');
/*!40000 ALTER TABLE `lezioni` ENABLE KEYS */;

-- Dump della struttura di tabella dibapp.materie
CREATE TABLE IF NOT EXISTS `materie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella dibapp.materie: ~2 rows (circa)
/*!40000 ALTER TABLE `materie` DISABLE KEYS */;
INSERT INTO `materie` (`id`, `name`) VALUES
	(1, 'Sviluppo mobile software'),
	(2, 'Analisi matematica');
/*!40000 ALTER TABLE `materie` ENABLE KEYS */;

-- Dump della struttura di tabella dibapp.presenze
CREATE TABLE IF NOT EXISTS `presenze` (
  `lezione` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella dibapp.presenze: ~34 rows (circa)
/*!40000 ALTER TABLE `presenze` DISABLE KEYS */;
INSERT INTO `presenze` (`lezione`, `user`) VALUES
	(1, 5),
	(1, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5),
	(2, 5);
/*!40000 ALTER TABLE `presenze` ENABLE KEYS */;

-- Dump della struttura di tabella dibapp.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella dibapp.role: ~2 rows (circa)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`) VALUES
	(1, 'student'),
	(4, 'professor'),
	(5, 'admin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dump della struttura di tabella dibapp.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `signup_date` datetime NOT NULL DEFAULT curdate(),
  `last_latitude` varchar(50) DEFAULT NULL,
  `last_longitude` varchar(50) DEFAULT NULL,
  `last_location_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella dibapp.users: ~3 rows (circa)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `password`, `name`, `lastname`, `role`, `signup_date`, `last_latitude`, `last_longitude`, `last_location_date`) VALUES
	(2, 'admin@dibapp.it', '*4ACFE3202A5FF5CF467898FC58AAB1D615029441', 'Admin', 'Admin', 3, '2019-12-28 00:00:00', NULL, NULL, NULL),
	(3, 'docente@dibapp.it', '*B91CB18D088ACC7393F6890C296BB20BBE731118', 'Mario', 'Rossi', 2, '2019-12-29 00:00:00', '40.95495495495496', '17.27305477818298', '2020-01-14 18:45:57'),
	(5, 'studente@dibapp.it', '*AA38851AB645CDE7B3491AACB6B05ECCBE436438', 'Cosimo', 'Lafera', 1, '2019-12-28 00:00:00', '40.95495495495496', '17.296912588650088', '2020-01-23 20:20:42');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

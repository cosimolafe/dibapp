<?php
include "db.php";


function insertUser($name,$lastname,$email,$password,$role){
    $pdo = Database::connect();
    $query = "SELECT * FROM users WHERE email = '$email'";
    $error = array();
    foreach ($pdo->query($query) as $row){
       $error["error"] = "USER_EXIST";
    }

    $pdo->exec("INSERT INTO users (name,lastname,email,password,role) VALUES('$name','$lastname','$email',PASSWORD('$password'),$role)");
    Database::disconnect();
    return $error;
}

function login($email,$password){
    $pdo = Database::connect();
    $user = array();
    $query = "SELECT * FROM users WHERE email = '$email' AND password = PASSWORD('$password')";
    foreach ($pdo->query($query) as $row){
        $user = $row;
    }
    Database::disconnect();
    return $user;
}


function getLezione($day){
    $pdo = Database::connect();
    $lezione = array();
    if($day == null || $day == ""){
        $query = "SELECT lezioni.id,lezioni.day,materie.name FROM lezioni JOIN materie ON lezioni.materia = materie.id 
        WHERE date(lezioni.day) = date(NOW())";
    }else{
        $query = "SELECT lezioni.id,lezioni.day,materie.name FROM lezioni JOIN materie ON lezioni.materia = materie.id 
    WHERE date(lezioni.day) = date('$day')";
    }
    foreach ($pdo->query($query) as $row){
        $lezione = $row;
    }
    Database::disconnect();
    return $lezione;
}

function getCommenti($lezione){
    $pdo = Database::connect();
    $commenti = array();
    $query = "SELECT * FROM commenti  WHERE lezione = $lezione";
    foreach ($pdo->query($query) as $row){
        $commenti[] = $row;
    }
    Database::disconnect();
    return $commenti;
}

function nuovoCommento($user,$testo,$voto,$pubblico,$lezione){
    $pdo = Database::connect();
    $query = "SELECT * FROM presenze WHERE user = (SELECT id from users WHERE email = '$user') AND lezione = $lezione";
    $error = 1;
    foreach ($pdo->query($query) as $row){
       $error = 0;
    }
    if($error == 0)
        $pdo->exec("INSERT INTO commenti VALUES($lezione,$voto,'$testo',$pubblico);"); 
    Database::disconnect();
    return $error;
}



function setUserLocation($user,$lat,$lon){
    $pdo = Database::connect();
    $pdo->exec("UPDATE users SET last_latitude = '$lat',last_longitude = '$lon', last_location_date = NOW() WHERE email = '$user'");
    Database::disconnect();
}

function cercaStudentiPresenti(){
    $pdo = Database::connect();
    $studenti = array();
    $query = "SELECT *  FROM users  WHERE role = 1 AND TIMESTAMPDIFF(MINUTE,last_location_date,NOW()) < 200";
    foreach ($pdo->query($query) as $row){
        $studenti[] = $row;
    }
    Database::disconnect();
    return $studenti;
}


function impostaPresenzaStudente($studente,$lezione){
    $pdo = Database::connect();
    $pdo->exec("INSERT INTO presenze (user,lezione) (SELECT id,$lezione FROM users WHERE email = '$studente')");
    Database::disconnect();
}

?>
<?php
include "functions.php";
$function =  $_POST["fun"];


if($function == 1){
    $res = insertUser($_POST["name"],$_POST["lastname"],$_POST["email"],$_POST["password"],$_POST["role"]);
    echo json_encode( $res);
}

if($function == 2){
    $res = login($_POST["email"],$_POST["password"]);
    echo json_encode( $res);
}

if($function == 3){
    $res = getLezione($_POST["giorno"]);
    echo json_encode( $res);
}

if($function == 4){
    $res = getCommenti($_POST["lezione"]);
    echo json_encode( $res);
}

if($function == 5){
    nuovoCommento($_POST["studente"],$_POST["text"],$_POST["voto"],$_POST["pubblico"],$_POST["lezione"]);
}

if($function == 6){
    setUserLocation($_POST["email"],$_POST["lat"],$_POST["lon"]);
}

if($function == 7){
    $res = cercaStudentiPresenti($_POST["lezione"],$_POST["lat"],$_POST["lon"]);
    echo json_encode( $res);
}


if($function == 8){
    impostaPresenzaStudente($_POST["studente"],$_POST["lezione"]);
}

?>
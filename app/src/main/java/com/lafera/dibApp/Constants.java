package com.lafera.dibApp;

public class Constants {
    public static final int ROLE_STUDENT = 1;
    public static final int ROLE_PROFESSOR = 2;
    public static final int ROLE_ADMIN = 3;

}

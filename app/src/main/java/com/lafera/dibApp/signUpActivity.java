package com.lafera.dibApp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.lafera.dibApp.data.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class signUpActivity extends AppCompatActivity {

    private Context ctx;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        ctx = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        final Button loginButton = findViewById(R.id.btn_signup);

        final Button backTolgoin = findViewById(R.id.btn_back_login);


        backTolgoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLoginActivity();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });
    }



    private void signup() {
        TextInputLayout text_name = findViewById(R.id.edit_name);
        TextInputLayout text_lastname = findViewById(R.id.edit_lastname);
        TextInputLayout text_email = findViewById(R.id.edit_email);
        TextInputLayout text_password = findViewById(R.id.edit_password);
        TextInputLayout text_password2 = findViewById(R.id.edit_password2);

        String password = text_password.getEditText().getText().toString();
        String confirm_password = text_password2.getEditText().getText().toString();
        if (password.equals(confirm_password)) {
            UserModel student = new UserModel();
            student.setName(text_name.getEditText().getText().toString());
            student.setLastname(text_lastname.getEditText().getText().toString());
            student.setEmail(text_email.getEditText().getText().toString());
            student.setPassword(text_password.getEditText().getText().toString());
            student.setRole(Constants.ROLE_STUDENT);
            final ServerTask singupTask = new ServerTask() {
                @Override
                public void onSuccess(Object obj) {
                    try{
                        String strJSON = (String) obj;
                        JSONArray json = null;
                        try {
                            if(strJSON != null){
                                JSONObject json_obj = (JSONObject) new JSONTokener(strJSON).nextValue();
                                if(json_obj.has("error")) {
                                    Session.currActivity.runOnUiThread(new Runnable() {
                                        public void run() {
                                            showErrpr(R.string.signup_failed);
                                        }
                                    });
                                }
                                else{
                                    Session.user = new UserModel();
                                    Session.user.setEmail(json_obj.getString("email"));
                                    Session.user.setLastname(json_obj.getString("lastname"));
                                    Session.user.setName(json_obj.getString("name"));
                                    Session.user.setRole(Integer.parseInt(json_obj.getString("role")));
                                    openMainActivity();
                                }
                            }
                        } catch (JSONException e) {
                            Session.currActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    showErrpr(R.string.signup_failed);
                                }
                            });
                        }
                    }catch (Exception e) {
                        Session.currActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                showErrpr(R.string.signup_failed);
                            }
                        });
                    }
                }
            };
            Server.insertUser(student, singupTask);
        }
    }

    private void openMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void openLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


    private void showErrpr(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}

package com.lafera.dibApp.data;

public class CommentiModel {
    private String message;
    private int rating;
    private boolean ispublic;

    public int getPubblico() {
        return pubblico;
    }

    public void setPubblico(int pubblico) {
        this.pubblico = pubblico;
    }

    private int pubblico;

    public int getLezione() {
        return lezione;
    }

    public void setLezione(int lezione) {
        this.lezione = lezione;
    }

    private int lezione;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isIspublic() {
        return ispublic;
    }

    public void setIspublic(boolean ispublic) {
        this.ispublic = ispublic;
    }
}

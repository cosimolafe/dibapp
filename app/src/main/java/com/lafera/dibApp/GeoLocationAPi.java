package com.lafera.dibApp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;


public class GeoLocationAPi {
    private FusedLocationProviderClient fusedLocationClient;
    private Location _location;
    public GeoLocationAPi (){
        checkPermissions();
    }

    public Double[] getLastLocation(){
        if(_location != null){
            Double[] l = {_location.getLatitude(),_location.getLongitude()};
            return l;
        }else
            return  null;

    }

    public void getLocation(){
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(Session.currActivity);
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(Session.currActivity, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                _location = location;
                                Double[] l = new Double[2];
                                l[0] = location.getLatitude();
                                l[1] = location.getLongitude();
                                Server.setUserLocation(Session.user,l);
                            }
                        }
                    });
    }


    public double caluclateDistance(double lat,double lon){
       Location dest = _location;
       dest.setLatitude(lat);
       dest.setLongitude(lon);
       return _location.distanceTo(dest);
    }


    private void checkPermissions(){
        boolean permissionAccessCoarseLocationApproved =
                Session.currActivity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;
        if (!permissionAccessCoarseLocationApproved) {
           Session.currActivity.requestPermissions( new String[] {
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    },
                   1);
        }
    }
}

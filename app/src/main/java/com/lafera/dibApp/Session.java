package com.lafera.dibApp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;

import com.lafera.dibApp.data.LezioneModel;
import com.lafera.dibApp.data.UserModel;

public class Session {

    public static UserModel user;
    public static Activity currActivity;
    public static LezioneModel lezione;
    public static GeoLocationAPi geolocation;
    public static String data_corrente;
}

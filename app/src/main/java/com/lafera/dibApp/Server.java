package com.lafera.dibApp;

import com.lafera.dibApp.data.CommentiModel;
import com.lafera.dibApp.data.LezioneModel;
import com.lafera.dibApp.data.UserModel;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.List;

public class Server {
        private static String address = "http://192.168.1.145/dibapp/api.php";




        public static void login(String email,String password,ServerTask task){
            List<NameValuePair> data =  new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("fun", "2"));
            data.add(new BasicNameValuePair("email", email));
            data.add(new BasicNameValuePair("password", password));
            sendRequest(data,task);
        }

    public static void getCommentiLezione(LezioneModel lezione, ServerTask task){
        List<NameValuePair> data =  new ArrayList<NameValuePair>();
        data.add(new BasicNameValuePair("fun", "4"));
        data.add(new BasicNameValuePair("lezione", lezione.getId()+""));
        sendRequest(data,task);
    }


    public static void getLezione(String giorno,ServerTask task){
        List<NameValuePair> list =  new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair("fun", "3"));
        list.add(new BasicNameValuePair("giorno", giorno));
        sendRequest(list,task);
    }


    public static void insertCommento(UserModel user,CommentiModel commento,ServerTask task){
        List<NameValuePair> data =  new ArrayList<NameValuePair>();
        data.add(new BasicNameValuePair("fun", "5"));
        data.add(new BasicNameValuePair("studente", user.getEmail()));
        data.add(new BasicNameValuePair("text", commento.getMessage()));
        data.add(new BasicNameValuePair("voto", commento.getRating()+""));
        data.add(new BasicNameValuePair("lezione", commento.getLezione()+""));
        data.add(new BasicNameValuePair("pubblico", commento.getPubblico()+""));
        sendRequest(data,task);
    }


    public static void setUserLocation(UserModel user,Double[] location){
        if(location != null){
            List<NameValuePair> data =  new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("fun", "6"));
            data.add(new BasicNameValuePair("email", user.getEmail()));
            data.add(new BasicNameValuePair("lat", location[0]+""));
            data.add(new BasicNameValuePair("lon", location[1]+""));
            sendRequest(data,null);
        }

    }


    public static void cercaPresenze(LezioneModel lezione,Double[] location,ServerTask task){
        if(location != null){
            List<NameValuePair> data =  new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("fun", "7"));
            data.add(new BasicNameValuePair("lezione", lezione.getId()+""));
            data.add(new BasicNameValuePair("lat", location[0]+""));
            data.add(new BasicNameValuePair("lon", location[1]+""));
            sendRequest(data,task);
        }
    }



    public static void impostaPresenza(UserModel user,LezioneModel lezione){
        List<NameValuePair> data =  new ArrayList<NameValuePair>();
        data.add(new BasicNameValuePair("fun", "8"));
        data.add(new BasicNameValuePair("studente", user.getEmail()+""));
        data.add(new BasicNameValuePair("lezione", lezione.getId()+""));
        sendRequest(data,null);
    }

        public static void insertUser(UserModel user,ServerTask task){
            List<NameValuePair> data =  new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("fun", "1"));
            data.add(new BasicNameValuePair("name", user.getName()));
            data.add(new BasicNameValuePair("lastname", user.getLastname()));
            data.add(new BasicNameValuePair("role", user.getRole()+""));
            data.add(new BasicNameValuePair("email", user.getEmail()));
            data.add(new BasicNameValuePair("password", user.getPassword()));
            sendRequest(data,task);
        }

        private static String sendRequest( final List<NameValuePair> data,final ServerTask task ) {
            Thread thread = new Thread(){
                public void run(){
                    try {
                        HttpClient client = new DefaultHttpClient();
                        HttpPost post = new HttpPost(address);
                        post.setEntity(new UrlEncodedFormEntity(data));
                        HttpResponse response = client.execute(post);
                        if(task != null)
                            task.onSuccess(EntityUtils.toString(response.getEntity(), "UTF-8"));
                        return;
                    } catch (UnsupportedEncodingException e) {
                        if(task != null)
                            task.onError(e);
                        e.printStackTrace();
                    } catch (ClientProtocolException e) {
                        if(task != null)
                            task.onError(e);
                        e.printStackTrace();
                    } catch (IOException e) {
                        if(task != null)
                            task.onError(e);
                        e.printStackTrace();
                    }

                }
            };

            thread.start();

            return null;
        }
}

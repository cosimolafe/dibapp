package com.lafera.dibApp.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.google.android.material.textfield.TextInputLayout;
import com.lafera.dibApp.Constants;
import com.lafera.dibApp.R;
import com.lafera.dibApp.Server;
import com.lafera.dibApp.ServerTask;
import com.lafera.dibApp.data.UserModel;

public class AdminFragment {

    private View root;
    public View init(@NonNull LayoutInflater inflater, ViewGroup container) {

        root = inflater.inflate(R.layout.fragment_admin, container, false);

        Button button = root.findViewById(R.id.btn_add_professor);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               addProfessor();
            }
        });
        return root;
    }

    private void addProfessor(){
        TextInputLayout text_name = root.findViewById(R.id.edit_name);
        TextInputLayout text_lastname = root.findViewById(R.id.edit_lastname);
        TextInputLayout text_email = root.findViewById(R.id.edit_email);
        TextInputLayout text_password= root.findViewById(R.id.edit_password);
        TextInputLayout text_password2= root.findViewById(R.id.edit_password2);

        String password = text_password.getEditText().getText().toString();
        String confirm_password = text_password2.getEditText().getText().toString();
        if(password.equals(confirm_password)){
            UserModel professor = new UserModel();
            professor.setName(text_name.getEditText().getText().toString());
            professor.setLastname(text_lastname.getEditText().getText().toString());
            professor.setEmail(text_email.getEditText().getText().toString());
            professor.setPassword(text_password.getEditText().getText().toString());
            professor.setRole(Constants.ROLE_PROFESSOR);
            ServerTask task = new ServerTask() {
                @Override
                public void onSuccess(Object obj) {
                    super.onSuccess(obj);
                }
            };
            Server.insertUser(professor,task);
        }





    }
}
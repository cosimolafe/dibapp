package com.lafera.dibApp.ui;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.lafera.dibApp.Constants;
import com.lafera.dibApp.R;
import com.lafera.dibApp.Server;
import com.lafera.dibApp.ServerTask;
import com.lafera.dibApp.Session;
import com.lafera.dibApp.data.LezioneModel;
import com.lafera.dibApp.data.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

public class HomeFragment extends Fragment {
    private View view;
    private LayoutInflater inflater;
    private ViewGroup container;




    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        this.container = container;
        this.inflater = inflater;
        if(Session.user.getRole() == Constants.ROLE_ADMIN){
            AdminFragment adminFragment = new AdminFragment();
            view = adminFragment.init(inflater,container);
        }else{
            view = inflater.inflate(R.layout.fragment_professor, container, false);
            init();

            FloatingActionButton fab = getActivity().findViewById(R.id.fab);
            getActivity().findViewById(R.id.fab).setVisibility(View.VISIBLE);
            fab.show();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String date_string = formatter.format(date);
            if(Session.data_corrente != null){
                try {
                    Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(Session.data_corrente);
                    if(!date_string.equals(formatter.format(date1))) {
                        fab.hide();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if(Session.user.getRole() == Constants.ROLE_PROFESSOR) {
                fab.setImageResource(R.drawable.ic_menu_refresh);
                cercaStudentiPresenti();
            }
            fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    //se docente allora cerco gli studenti presenti
                    if(Session.user.getRole() == Constants.ROLE_PROFESSOR){
                        cercaStudentiPresenti();
                        loadCommenti();
                    }else{
                        //se studente allora mostro gli input per l'invio dei commenti
                        CommentoFragment nextFrag= new CommentoFragment();
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.nav_host_fragment, nextFrag, "findThisFragment")
                                .addToBackStack(null)
                                .commit();

                        Fragment someFragment = new CommentoFragment();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.nav_host_fragment, someFragment ); // give your fragment container id in first parameter
                        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
                        transaction.commit();
                    }
                }
            });
        }




        return view;
    }


    private void init(){

        ServerTask task = new ServerTask() {
            @Override
            public void onSuccess(Object obj) {
                String strJSON = (String) obj;
                try {
                    if (strJSON != null) {
                        JSONObject json_obj = (JSONObject) new JSONTokener(strJSON).nextValue();
                        Session.lezione = new LezioneModel();
                        if(json_obj.has("id")){
                                Session.lezione.setId(Integer.parseInt(json_obj.getString("id")));
                                Session.lezione.setName(json_obj.getString("name"));
                                Session.currActivity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        TextView title = view.findViewById(R.id.text_home);
                                        title.setText(Session.lezione.getName());
                                    }
                                });
                        }else{
                            Session.currActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    TextView title = view.findViewById(R.id.text_home);
                                    title.setText(R.string.no_lezione);
                                    Session.lezione = null;
                                }
                            });
                        }

                    }

                } catch (Exception e) {
                    Session.currActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            TextView title = view.findViewById(R.id.text_home);
                            title.setText(R.string.no_lezione);
                            Session.lezione = null;
                        }
                    });
                    e.printStackTrace();
            }//Carico e visualizzo la lista dei commenti
            loadCommenti();
            }
        };
        if(Session.data_corrente == null) Session.data_corrente= "";
        Server.getLezione(Session.data_corrente,task);
    }



    public void loadCommenti(){
        ServerTask serverTask = new ServerTask() {
            @Override
            public void onSuccess(Object obj) {
                String strJSON = (String) obj;
                System.out.println(strJSON);
                try {
                    if(strJSON != null){

                        JSONArray json_array = (JSONArray) new JSONTokener(strJSON).nextValue();
                        final ArrayList<String> commenti = new ArrayList<String>();
                        final ArrayList<Integer> voti = new ArrayList<Integer>();
                        for(int i = 0; i < json_array.length();i++){
                            //se messaggio privato e utente loggato è uno studente non mostro il commento
                            if(!json_array.getJSONObject(i).getString("pubblico").equals("0") || Session.user.getRole() != Constants.ROLE_STUDENT){
                                commenti.add(json_array.getJSONObject(i).getString("messaggio"));
                                voti.add(Integer.parseInt(json_array.getJSONObject(i).getString("valutazione")));
                            }

                        }
                        Session.currActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                showCommenti(commenti,voti);
                            }
                        });

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        if(Session.lezione != null)
            Server.getCommentiLezione(Session.lezione,serverTask);

    }


    private void showCommenti(ArrayList<String> commenti,ArrayList<Integer> voti){
        CustomAdapter CustomAdapter = new CustomAdapter(Session.currActivity, commenti, voti);
        ListView listView = (ListView) view.findViewById(R.id.list_commenti);
        listView.setAdapter(CustomAdapter);
    }


    //ricerca degli studenti presenti in aula
    private void cercaStudentiPresenti(){
        Session.geolocation.getLocation();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ServerTask task = new ServerTask() {
                @Override
                public void onSuccess(Object obj) {
                    String strJSON = (String) obj;
                    try {
                        if(strJSON != null){

                            JSONArray json_array = (JSONArray) new JSONTokener(strJSON).nextValue();
                            final ArrayList<UserModel> studenti = new ArrayList<UserModel>();
                            for(int i = 0; i < json_array.length();i++){
                                String last_lat = json_array.getJSONObject(i).getString("last_latitude");
                                String last_lon = json_array.getJSONObject(i).getString("last_longitude");
                                Double lat = Double.parseDouble(last_lat);
                                Double lon = Double.parseDouble(last_lon);
                                Double distance = Session.geolocation.caluclateDistance(lat,lon);
                                if(distance < 10){
                                    UserModel st = new UserModel();
                                    st.setEmail(json_array.getJSONObject(i).getString("email"));
                                    st.setName(json_array.getJSONObject(i).getString("name"));
                                    st.setLastname(json_array.getJSONObject(i).getString("lastname"));
                                    studenti.add(st);
                                    Server.impostaPresenza(st,Session.lezione);
                                }
                            }
                            Session.currActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    String text = Session.currActivity.getString(R.string.no_studenti);
                                    if(studenti.size() == 1){
                                        text = Session.currActivity.getString(R.string.one_student);
                                    }else{
                                        if(studenti.size() > 0) {
                                            text = Session.currActivity.getString(R.string.student_number);
                                            text = text.replace("%n",studenti.size()+"");
                                        }
                                    }
                                    TextView t = Session.currActivity.findViewById(R.id.text_students);
                                    t.setText(text);
                                    Toast.makeText(Session.currActivity,text,Toast.LENGTH_LONG).show();
                                }
                            });

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            if(Session.lezione != null)
                Server.cercaPresenze(Session.lezione,Session.geolocation.getLastLocation(),task);
    }




}
package com.lafera.dibApp.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.lafera.dibApp.R;

import java.util.ArrayList;

/**
 * CustomAdapter
 * Adapter custom per la visualizzazione dei commenti lasciati dagli studenti
 */
class CustomAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> messaggi;
    ArrayList<Integer> voti;
    LayoutInflater inflter;

    public CustomAdapter(Context applicationContext, ArrayList<String> messaggi, ArrayList<Integer> voti) {
        this.context = context;
        this.messaggi = messaggi;
        this.voti = voti;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return messaggi.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.row_list_commenti, null);
        RatingBar rating = view.findViewById(R.id.ratingBar);
        TextView text =  view.findViewById(R.id.textView3);
        text.setText(messaggi.get(i));
        rating.setRating(voti.get(i));
        return view;
    }
}

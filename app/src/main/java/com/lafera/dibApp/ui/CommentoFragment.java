package com.lafera.dibApp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.textfield.TextInputLayout;
import com.lafera.dibApp.LoginActivity;
import com.lafera.dibApp.MainActivity;
import com.lafera.dibApp.R;
import com.lafera.dibApp.Server;
import com.lafera.dibApp.ServerTask;
import com.lafera.dibApp.Session;
import com.lafera.dibApp.data.CommentiModel;

public class CommentoFragment   extends Fragment {
    private View view;
    private LayoutInflater inflater;
    private ViewGroup container;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        this.container = container;
        this.inflater = inflater;
        view = inflater.inflate(R.layout.fragment_commento, container, false);
        Button button = view.findViewById(R.id.btn_send_message);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salvaCommento();
            }
        });
        getActivity().findViewById(R.id.fab).setVisibility(View.INVISIBLE);
        Button cancel = view.findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMainActivity();
            }
        });

        return view;
    }

    private void salvaCommento(){
                CommentiModel commento = new CommentiModel();
                TextInputLayout text = view.findViewById(R.id.edit_commento);
                Switch ispublic = view.findViewById(R.id.switch1);
                RatingBar rating = view.findViewById(R.id.ratingBar);
                int voto = (int)rating.getRating();
                String t = text.getEditText().getText().toString();
                if(voto == 0 || t.equals("")){
                    Session.currActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(Session.currActivity,"Completa tutti i campi",Toast.LENGTH_LONG).show();
                        }
                    });

                }else{
                    ServerTask task = new ServerTask() {
                        @Override
                        public void onSuccess(Object obj) {
                            System.out.println((String)obj);
                            openMainActivity();
                        }
                    };
                    commento.setLezione(Session.lezione.getId());
                    commento.setMessage(t);
                    commento.setRating(voto);
                    if(ispublic.isChecked())
                        commento.setPubblico(1);
                    else
                        commento.setPubblico(0);
                    Server.insertCommento(Session.user,commento,task);
        }
    }

    private void openMainActivity(){
        Intent intent = new Intent(Session.currActivity, MainActivity.class);
        startActivity(intent);
    }
}

package com.lafera.dibApp;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;


import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.lafera.dibApp.data.UserModel;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


public class LoginActivity extends AppCompatActivity {

    private Context ctx;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        ctx = this;
        super.onCreate(savedInstanceState);
        Session.currActivity = this;
        Session.geolocation = new GeoLocationAPi();
        setContentView(R.layout.activity_login);

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);



        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login(usernameEditText.getText().toString(),passwordEditText.getText().toString());
            }
        });

        final Button signupButton = findViewById(R.id.login_signup);
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSignUpActivity();
            }
        });

        SharedPreferences shared = getSharedPreferences("dibapp", Context.MODE_PRIVATE);
        if(!shared.getString("email","").isEmpty()){
            login(shared.getString("email",""),shared.getString("password",""));
        }


    }


    private void login(final String username,final String password){
        final ServerTask loginTask = new ServerTask() {
            @Override
            public void onError(Object obj) {
                SharedPreferences sharedpreferences = getSharedPreferences("dibapp", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
            }
            @Override
            public void onSuccess(Object obj) {
                try{
                    String strJSON = (String) obj;
                    try {
                        if(strJSON != null){
                            JSONObject json_obj = (JSONObject) new JSONTokener(strJSON).nextValue();
                            if(json_obj.has("error")){
                                Session.currActivity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        showLoginFailed(R.string.login_failed);
                                    }
                                });
                            }
                            else{
                                Session.user = new UserModel();
                                Session.user.setEmail(json_obj.getString("email"));
                                Session.user.setLastname(json_obj.getString("lastname"));
                                Session.user.setName(json_obj.getString("name"));
                                Session.user.setRole(Integer.parseInt(json_obj.getString("role")));

                                //Salvo i dati del login all'interno dello sharedPreference per il login automatico
                                SharedPreferences sharedpreferences = getSharedPreferences("dibapp", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("email", username);
                                editor.putString("password",password);
                                editor.commit();

                                openMainActivity();
                            }
                        }
                    } catch (JSONException e) {
                        Session.currActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                showLoginFailed(R.string.login_failed);
                            }
                        });
                    }
                }catch (Exception e) {
                    Session.currActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            showLoginFailed(R.string.login_failed);
                        }
                    });

                }
            }
        };
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);
        loadingProgressBar.setVisibility(View.VISIBLE);
        Server.login(username,password,loginTask);
        loadingProgressBar.setVisibility(View.INVISIBLE);
    }

    private void openMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    private void openSignUpActivity(){
        Intent intent = new Intent(this, signUpActivity.class);
        startActivity(intent);
    }


    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}
